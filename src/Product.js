import React, { Component } from "react";

class Product extends Component{
    render(){
        return(
            <div className="col-sm-4 mt-3">
                <div className="col-sm-12 border ml-2">
                    <p>{ this.props.name }</p>
                    <p>{ this.props.price } VND</p>
                    <button type="button" className="btn btn-success">Add to Cart</button>
                </div>
            </div>
        );
    }
}
export default Product;