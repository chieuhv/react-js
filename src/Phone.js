import React, { Component } from "react";

import Product from "./Product";

class Phone extends Component{

    //create constructor
    constructor(props){
        super(props);
        
        this.state = {
            id: "",
            name_p: "",
            price_p: ""
        }
    }

    //get value from input to set state
    onHandleChange = (event) =>{
        var target = event.target;
        var name = target.name;
        var value = target.value;

        this.setState({
            [name] : value
        });
    }

    addProduct = (event) => {
        let array = [];
        let news = {id: Math.random, name: this.state.name_p, price: this.state.price_p}
        array.push(news);

        event.preventDefault();
    }

    render(){
        return(
            <div className="col-sm-6 mt-5">
                <form method="post" className="col-sm-12" onSubmit = { this.addProduct }>
                    <div className="form-group">
                        <label>Name Product</label>
                        <input type="text" className="form-control" name="name_p" placeholder="Enter product name" onChange={ this.onHandleChange }/>
                    </div>
                    <div className="form-group">
                        <label>Price</label>
                        <input type="text" className="form-control" name="price_p" placeholder="Enter product price" onChange={ this.onHandleChange }/>
                    </div>
                    <button type="submit" className="btn btn-primary">Add product</button>
                </form>
                <div>
                    <Product id = { this.state.id} name = {this.state.name_p} price = { this.state.price_p}></Product>
                </div>
            </div>   
        );
    }
}
export default Phone;