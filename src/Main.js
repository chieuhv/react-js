import React, { Component } from "react";

import Product from "./Product";
import Phone from "./Phone";

class Main extends Component{

    render(){
        const listProduct = [
            {
                id: 1,
                name: "IPhone 11 Pro",
                price: "20,000,000"
            },
            {
                id: 2,
                name: "IPhone 8 Plus",
                price: "8,000,000"
            },
            {
                id: 3,
                name: "IPhone XS Max",
                price: "10,000,000"
            }
        ]
        
        let elements = listProduct.map((product) => {
            return <Product key = { product.id } name = { product.name } price = { product.price }></Product>
        });

        return(
            <div className = "container">
                <div className="row mt-3 d-flex">
                    { elements }
                </div>
                <div className="row">
                    <Phone/>
                </div>
            </div>
        );
    }
}
export default Main;